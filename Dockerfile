FROM python:3.7.8-slim-buster
COPY src /src
WORKDIR /src
CMD python main.py /src/tests/data/input.txt