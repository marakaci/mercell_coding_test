## This repo has a solution to a bowling game task

#### The solution is an app that accepts a file path of comma-separated rolls and produces the output by turns with total score.

### Structure:
1) /src/main.py - entrypoint
2) /src/bowling_score_calculator.py - pipeline builder
3) /src/argument_parser.py - handles args
4) /src/input_file_reader.py - reads the file
5) /src/input_parser.py - parses input to turn objects
6) /src/total_score_calculator.py - calculates total score
7) /src/output_handler.py - prints the turns and total score
8) /src/test - unit and integrational test cases
9) /src/test/data - data for testing

### Run 
#### Using docker
In order to test the solution you need to have docker installed
https://docs.docker.com/get-docker/

1) build the image with: 
   
   `docker build . -t bowling_calculator`
2) provide the volume of the file needed to be processed and run the container:

    `docker run -v $LOCAL_PATH:$PATH bowling_calculator python main.py $PATH`

    example:
    
    `docker run -v /home/nikita/projects/mercell_coding_test/input.txt:/input.txt bowling_calculator python main.py /input.txt`

#### Using local python 3.7+
1) no libs are required, bare python, just run the command with file path:
`python3.7 src/main.py $PATH`
