import sys

from argument_parser import ArgumentParser
from bowling_score_calculator import BowlingScoreCalculator
from input_file_reader import InputFileReader
from input_parser import InputParser
from output_handler import OutputHandler
from total_score_calculator import TotalScoreCalculator

if __name__ == "__main__":
    # list of args for the first step
    first_input = sys.argv[1:]
    arg_parser = ArgumentParser()
    input_file_reader = InputFileReader()
    input_parser = InputParser()
    total_score_calculator = TotalScoreCalculator()
    turn_output_handler = OutputHandler()
    calculator = BowlingScoreCalculator(
        arg_parser=arg_parser,
        input_file_reader=input_file_reader,
        input_parser=input_parser,
        total_score_calculator=total_score_calculator,
        output_handler=turn_output_handler,
        first_input=first_input
    )
    calculator.run()
