import os
from typing import List, Tuple

from pipeline import Step
from turn import AbstractTurn, LastTurn


class OutputHandler(Step):
    """Calculates total score of the turns"""
    SEPARATOR = "|"
    STRIKE_SING = "X"
    SPARE_SIGN = "/"
    GUTTER_SIGN = "-"

    def _get_header(self, turns_count: int) -> str:
        prefix = f"{self.SEPARATOR} "
        postfix = f"   {self.SEPARATOR}"
        turns_letters = [f"f{x + 1}" for x in range(turns_count)]
        return prefix + f" {self.SEPARATOR} ".join(turns_letters) + postfix

    def _get_turns_score_line(self, turns: List[AbstractTurn]):
        prefix = self.SEPARATOR
        postfix = self.SEPARATOR
        turn_lines = []
        for turn in turns[:-1]:
            turn_line = self._get_turn_line(turn)
            turn_lines.append(turn_line)
        last_turn_line = self._get_last_turn_line(turns[-1])
        turn_lines.append(last_turn_line)
        return prefix + self.SEPARATOR.join(turn_lines) + postfix

    def _get_turn_line(self, turn):
        if turn.is_strike:
            turn_line = f"{self.STRIKE_SING}   "
        else:
            if turn.is_first_gutter:
                first_value = self.GUTTER_SIGN
            else:
                first_value = turn.first_roll

            if turn.is_spare:
                second_value = self.SPARE_SIGN
            elif turn.is_second_gutter:
                second_value = self.GUTTER_SIGN
            else:
                second_value = turn.second_roll

            turn_line = f"{first_value}, {second_value}"
        return turn_line

    def _get_last_turn_line(self, turn: LastTurn):
        if not turn.is_strike and not turn.is_spare:
            # first two values like usual and spaces for the third one
            turn_line = self._get_turn_line(turn) + "   "
        else:
            if turn.is_strike:
                first_value = self.STRIKE_SING
            elif turn.is_first_gutter:
                first_value = self.GUTTER_SIGN
            else:
                first_value = str(turn.first_roll)

            if turn.is_second_strike:
                second_value = self.STRIKE_SING
            elif turn.is_spare:
                second_value = self.SPARE_SIGN
            elif turn.is_second_gutter:
                second_value = self.GUTTER_SIGN
            else:
                second_value = str(turn.second_roll)

            if turn.is_third_strike:
                third_value = self.STRIKE_SING
            elif turn.is_second_spare:
                third_value = self.SPARE_SIGN
            elif turn.is_third_gutter:
                third_value = self.GUTTER_SIGN
            else:
                third_value = str(turn.third_roll)
            turn_line = ", ".join([first_value, second_value, third_value])
        return turn_line

    def _get_total_score_line(self, total_score: int):
        return f"score: {total_score}"

    def handle(self, turns_with_score: Tuple[List[AbstractTurn], int]):
        turns = turns_with_score[0]
        score = turns_with_score[1]
        self.print_score(turns, score)

    def print_score(self, turns: List[AbstractTurn], score: int):
        lines_to_print = self.get_print_lines(turns, score)
        print(lines_to_print)

    def get_print_lines(self, turns: List[AbstractTurn], total_score: int):
        header = self._get_header(len(turns))
        score_line = self._get_turns_score_line(turns)
        total_score_line = self._get_total_score_line(total_score)
        return os.linesep.join([header, score_line, total_score_line])
