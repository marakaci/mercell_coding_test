import abc


class Step(abc.ABC):
    def handle(self, input):
        raise NotImplemented() # pragma: no cover


class Pipeline:
    def __init__(self, first_input=None):
        self.steps = []
        self.first_input = first_input

    def add_step(self, step: Step):
        self.steps.append(step)

    def run(self):
        input = self.first_input
        for step in self.steps:
            # output of a step is an input for the next step
            output = step.handle(input)
            input = output
        return input
