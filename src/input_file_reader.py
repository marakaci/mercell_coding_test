from pipeline import Step


class InputFileReader(Step):
    """Reads the game input file and returns the input data"""

    def handle(self, file_path) -> str:
        # first step, no input
        output = self.read_content(file_path)
        return output

    def read_content(self, file_path: str) -> str:
        with open(file_path, "r") as f:
            return f.readline()
