from typing import List

from pipeline import Step
from turn import AbstractTurn


class TotalScoreCalculator(Step):
    """Calculates total score of the turns"""

    def _is_last_turn(self, turns: List[AbstractTurn], current_index: int):
        return len(turns) == current_index + 1

    def _is_next_turn_last(self, turns: List[AbstractTurn], current_index: int):
        return len(turns) == current_index + 2

    def _add_spare_bonus_if_needed(self, turn: AbstractTurn, turns: List[AbstractTurn], current_index: int):
        if self._is_last_turn(turns, current_index):
            # for the last turn we just roll another boll in the turn
            return 0
        if turn.is_spare:
            next_turn = turns[current_index + 1]
            return next_turn.first_roll
        return 0

    def _add_strike_bonus_if_needed(self, turn: AbstractTurn, turns: List[AbstractTurn], current_index: int):
        if self._is_last_turn(turns, current_index):
            # for the last turn we just roll 2 more boll in the turn
            return 0
        if turn.is_strike:
            next_turn = turns[current_index + 1]
            first_bonus_part = next_turn.first_roll
            if next_turn.is_strike:
                # special case as we need to go 2 turns ahead to calculate bonus
                if self._is_next_turn_last(turns, current_index):
                    # if last turn is strike, it will still have second roll
                    second_bonus_part = next_turn.second_roll
                else:
                    second_next_turn = turns[current_index + 2]
                    second_bonus_part = second_next_turn.first_roll or 0
            else:
                second_bonus_part = next_turn.second_roll
            return first_bonus_part + second_bonus_part
        return 0

    def handle(self, turns: List[AbstractTurn]) -> (List[AbstractTurn], int):
        total_score = self.calculate_total_score(turns)
        return turns, total_score

    def calculate_total_score(self, turns: List[AbstractTurn]) -> int:
        score = 0
        for ind, turn in enumerate(turns):
            score += turn.calculate_total_point()
            score += self._add_spare_bonus_if_needed(turn, turns, ind)
            score += self._add_strike_bonus_if_needed(turn, turns, ind)
        return score
