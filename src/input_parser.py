from typing import List

from pipeline import Step
from turn import BasicTurn, LastTurn, AbstractTurn


class InputParser(Step):
    """Receives the game data and parses it to Turns"""
    MAX_TURNS = 10

    def _parse_input_str_to_values(self, input: str) -> List[int]:
        """input is comma and space separated numbers"""
        return [int(x) for x in input.split(", ")]

    def _is_next_turn_last(self, turns):
        return len(turns) == self.MAX_TURNS - 1

    def _get_current_turn(self, current_turn, all_turns):
        """either works with the current turn, or produces a new one if it's None"""
        if current_turn:
            return current_turn
        if self._is_next_turn_last(all_turns):
            new_turn = LastTurn()
        else:
            new_turn = BasicTurn()
        return new_turn

    def _add_roll_to_last_turn(self, roll: int, turn: LastTurn):
        if turn.first_roll is None:
            turn.first_roll = roll
        elif turn.second_roll is None:
            turn.second_roll = roll
        elif turn.third_roll is None and (turn.is_strike or turn.is_spare):
            turn.third_roll = roll
        else:
            raise RuntimeError(f"cannot set a roll to the last turn, turn: {turn}, roll: {roll}")

    def _add_roll_to_turn(self, roll: int, turn: BasicTurn):
        if turn.first_roll is None:
            turn.first_roll = roll
        elif turn.second_roll is None and not turn.is_strike:
            turn.second_roll = roll
        else:
            raise RuntimeError(f"cannot set a roll to a turn, turn: {turn}, roll: {roll}")

    def _should_add_turn_to_list(self, turn: BasicTurn):
        """if turn is populated, it should be added"""
        if turn.is_strike:
            return True
        if turn.is_spare:
            return True
        if turn.first_roll is not None and turn.second_roll is not None:
            return True
        return False

    def handle(self, input: str) -> List[AbstractTurn]:
        output = self.parse_input_to_turns(input)
        return output

    def parse_input_to_turns(self, input) -> List[AbstractTurn]:
        rolls = self._parse_input_str_to_values(input)
        turns = list()
        current_turn = None
        for roll in rolls:
            current_turn = self._get_current_turn(current_turn, turns)
            if self._is_next_turn_last(turns):
                self._add_roll_to_last_turn(roll, current_turn)
            else:
                self._add_roll_to_turn(roll, current_turn)
                if self._should_add_turn_to_list(current_turn):
                    turns.append(current_turn)
                    current_turn = None
        # append the last turn
        turns.append(current_turn)
        return turns
