from typing import List

from argument_parser import ArgumentParser
from input_file_reader import InputFileReader
from input_parser import InputParser
from output_handler import OutputHandler
from pipeline import Pipeline
from total_score_calculator import TotalScoreCalculator


class BowlingScoreCalculator:
    """Combine multiple steps to calculate bowling score"""

    def __init__(
            self,
            first_input: List[str],
            arg_parser: ArgumentParser,
            input_file_reader: InputFileReader,
            input_parser: InputParser,
            total_score_calculator: TotalScoreCalculator,
            output_handler: OutputHandler,
    ):
        """
        Creates a pipeline of the steps in the following order
        arg_parser -> input_file_reader -> input_parser -> total_score_calculator -> output_handler
        first_input is used to be passed to the first step
        """
        self.pipeline = Pipeline(first_input)
        self.pipeline.add_step(arg_parser)
        self.pipeline.add_step(input_file_reader)
        self.pipeline.add_step(input_parser)
        self.pipeline.add_step(total_score_calculator)
        self.pipeline.add_step(output_handler)

    def run(self):
        self.pipeline.run()
