import unittest

from total_score_calculator import TotalScoreCalculator
from turn import BasicTurn, LastTurn


class TestTotalScoreCalculator(unittest.TestCase):
    def setUp(self) -> None:
        self.total_score_calculator = TotalScoreCalculator()
        self.basic_first_roll = 2
        self.basic_second_roll = 3
        turns = []
        for x in range(9):
            turn = BasicTurn()
            turn.first_roll = self.basic_first_roll
            turn.second_roll = self.basic_second_roll
            turns.append(turn)
        turn = LastTurn()
        turn.first_roll = self.basic_first_roll
        turn.second_roll = self.basic_second_roll
        turns.append(turn)
        self.turns = turns
        self.basic_total = (self.basic_first_roll + self.basic_second_roll) * 10

    def test_handle_basic(self):
        _, total = self.total_score_calculator.handle(self.turns)
        self.assertEqual(total, self.basic_total)

    def test_handle_all_strikes(self):
        turns = []
        for x in range(9):
            turn = BasicTurn()
            turn.first_roll = BasicTurn.ROLL_MAX_VALUE
            turns.append(turn)
        turn = LastTurn()
        turn.first_roll = LastTurn.ROLL_MAX_VALUE
        turn.second_roll = LastTurn.ROLL_MAX_VALUE
        turn.third_roll = LastTurn.ROLL_MAX_VALUE
        turns.append(turn)
        _, total = self.total_score_calculator.handle(turns)
        self.assertEqual(total, 300)

    def test_handle_strikes_and_spares(self):
        turns = self.turns[:]
        turn = BasicTurn()
        turn.first_roll = BasicTurn.ROLL_MAX_VALUE - 1
        turn.second_roll = BasicTurn.ROLL_MAX_VALUE - turn.first_roll
        turns[8] = turn
        turn = LastTurn()
        turn.first_roll = BasicTurn.ROLL_MAX_VALUE - 1
        turn.second_roll = BasicTurn.ROLL_MAX_VALUE - turn.first_roll
        turn.third_roll = 3
        turns[9] = turn

        turn = BasicTurn()
        turn.first_roll = BasicTurn.ROLL_MAX_VALUE
        turns[3] = turn

        _, total = self.total_score_calculator.handle(turns)
        total = self.basic_total
        # bare plus for spares and strikes
        total += (BasicTurn.ROLL_MAX_VALUE - (self.basic_first_roll + self.basic_second_roll)) * 3
        # bonuses for spares
        total += turns[9].first_roll + self.basic_first_roll
        # bonus for last spare
        total += turns[9].third_roll
        # bonuses for strikes
        total += self.basic_first_roll + self.basic_second_roll
        self.assertEqual(total, total)
