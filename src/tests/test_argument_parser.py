import os
import unittest

from argument_parser import ArgumentParser


class TestArgumentPaser(unittest.TestCase):
    def setUp(self) -> None:
        self.argument_parser = ArgumentParser()

    def test_handle(self):
        dirname = os.path.dirname(__file__)
        test_file_path = os.path.join(dirname, 'data/input.txt')
        output = self.argument_parser.handle([test_file_path])
        self.assertEqual(output, test_file_path)
