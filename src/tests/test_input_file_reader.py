import os
import unittest

from input_file_reader import InputFileReader


class TestInputFileReader(unittest.TestCase):
    def setUp(self) -> None:
        self.game_input_file_reader = InputFileReader()

    def test_handle(self):
        dirname = os.path.dirname(__file__)
        test_file_path = os.path.join(dirname, 'data/input.txt')
        output = self.game_input_file_reader.handle(test_file_path)
        expected_output = "2, 3, 5, 4, 9, 1, 2, 5, 3, 2, 4, 2, 3, 3, 4, 6, 10, 3, 2"
        self.assertEqual(output, expected_output)
