import io
import os
import unittest
import unittest.mock

from argument_parser import ArgumentParser
from bowling_score_calculator import BowlingScoreCalculator
from input_file_reader import InputFileReader
from input_parser import InputParser
from output_handler import OutputHandler
from total_score_calculator import TotalScoreCalculator


class TestMain(unittest.TestCase):
    def setUp(self) -> None:
        self.arg_parser = ArgumentParser()
        self.input_file_reader = InputFileReader()
        self.input_parser = InputParser()
        self.total_score_calculator = TotalScoreCalculator()
        self.turn_output_handler = OutputHandler()
        self.header_expected_line = "| f1 | f2 | f3 | f4 | f5 | f6 | f7 | f8 | f9 | f10   |"


    def test_integration_run_basic(self):
        dirname = os.path.dirname(__file__)
        test_file_path = os.path.join(dirname, 'data/input.txt')
        calculator = BowlingScoreCalculator(
            arg_parser=self.arg_parser,
            input_file_reader=self.input_file_reader,
            input_parser=self.input_parser,
            total_score_calculator=self.total_score_calculator,
            output_handler=self.turn_output_handler,
            first_input=[test_file_path]
        )
        score_line = f"|2, 3|5, 4|9, /|2, 5|3, 2|4, 2|3, 3|4, /|X   |3, 2   |"
        total_expected_line = f"score: 90"
        expected_output = os.linesep.join([self.header_expected_line, score_line, total_expected_line])
        expected_output += os.linesep  # autoadded by print

        with unittest.mock.patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
            calculator.run()
            self.assertEqual(mock_stdout.getvalue(), expected_output)

    def test_integration_run_gutters_and_strikes(self):
        dirname = os.path.dirname(__file__)
        test_file_path = os.path.join(dirname, 'data/input_2.txt')
        calculator = BowlingScoreCalculator(
            arg_parser=self.arg_parser,
            input_file_reader=self.input_file_reader,
            input_parser=self.input_parser,
            total_score_calculator=self.total_score_calculator,
            output_handler=self.turn_output_handler,
            first_input=[test_file_path]
        )
        score_line = f"|-, 3|5, -|9, /|2, 5|3, 2|4, 2|3, 3|4, /|X   |X, 2, 5|"
        total_expected_line = f"score: 103"
        expected_output = os.linesep.join([self.header_expected_line, score_line, total_expected_line])
        expected_output += os.linesep  # autoadded by print

        with unittest.mock.patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
            calculator.run()
            self.assertEqual(mock_stdout.getvalue(), expected_output)

    def test_integration_run_spares(self):
        dirname = os.path.dirname(__file__)
        test_file_path = os.path.join(dirname, 'data/input_3.txt')
        calculator = BowlingScoreCalculator(
            arg_parser=self.arg_parser,
            input_file_reader=self.input_file_reader,
            input_parser=self.input_parser,
            total_score_calculator=self.total_score_calculator,
            output_handler=self.turn_output_handler,
            first_input=[test_file_path]
        )
        score_line = f"|7, 1|5, /|2, 7|4, /|-, 5|8, /|8, 1|4, 3|2, 4|5, 2   |"
        total_expected_line = f"score: 91"
        expected_output = os.linesep.join([self.header_expected_line, score_line, total_expected_line])
        expected_output += os.linesep  # autoadded by print

        with unittest.mock.patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
            calculator.run()
            self.assertEqual(mock_stdout.getvalue(), expected_output)
