import io
import os
import unittest.mock

from output_handler import OutputHandler
from turn import BasicTurn, LastTurn


class TestOutputHandler(unittest.TestCase):
    def setUp(self) -> None:
        self.output_handler = OutputHandler()
        self.basic_first_roll = 2
        self.basic_second_roll = 3
        turns = []
        for x in range(9):
            turn = BasicTurn()
            turn.first_roll = self.basic_first_roll
            turn.second_roll = self.basic_second_roll
            turns.append(turn)
        turn = LastTurn()
        turn.first_roll = self.basic_first_roll
        turn.second_roll = self.basic_second_roll
        turns.append(turn)
        self.turns = turns
        self.basic_total = (self.basic_first_roll + self.basic_second_roll) * 10
        self.header_expected_line = "| f1 | f2 | f3 | f4 | f5 | f6 | f7 | f8 | f9 | f10   |"
        self.total_expected_line = f"score: {self.basic_total}"

    def test_handle_basic(self):

        score_line = f"|{self.basic_first_roll}, {self.basic_second_roll}" * 9
        score_line += f"|{self.basic_first_roll}, {self.basic_second_roll}   |"
        expected_output = os.linesep.join([self.header_expected_line, score_line, self.total_expected_line])
        expected_output += os.linesep  # autoadded by print

        with unittest.mock.patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
            self.output_handler.handle((self.turns, self.basic_total))
            self.assertEqual(mock_stdout.getvalue(), expected_output)

    def test_handle_all_strikes(self):
        turns = []
        for x in range(9):
            turn = BasicTurn()
            turn.first_roll = BasicTurn.ROLL_MAX_VALUE
            turns.append(turn)
        turn = LastTurn()
        turn.first_roll = LastTurn.ROLL_MAX_VALUE
        turn.second_roll = LastTurn.ROLL_MAX_VALUE
        turn.third_roll = LastTurn.ROLL_MAX_VALUE
        turns.append(turn)

        strike_sign = self.output_handler.STRIKE_SING
        score_line = f"|{strike_sign}   " * 9
        score_line += f"|{strike_sign}, {strike_sign}, {strike_sign}|"
        expected_output = os.linesep.join([self.header_expected_line, score_line, self.total_expected_line])
        expected_output += os.linesep  # autoadded by print
        with unittest.mock.patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
            self.output_handler.handle((turns, self.basic_total))
            self.assertEqual(mock_stdout.getvalue(), expected_output)

    def test_handle_strikes_and_spares(self):
        turns = self.turns[:]
        turn = BasicTurn()
        turn.first_roll = BasicTurn.ROLL_MAX_VALUE - 1
        turn.second_roll = BasicTurn.ROLL_MAX_VALUE - turn.first_roll
        turns[8] = turn
        turn = LastTurn()
        turn.first_roll = BasicTurn.ROLL_MAX_VALUE - 1
        turn.second_roll = BasicTurn.ROLL_MAX_VALUE - turn.first_roll
        turn.third_roll = 3
        turns[9] = turn

        turn = BasicTurn()
        turn.first_roll = BasicTurn.ROLL_MAX_VALUE
        turns[3] = turn

        strike_sign = self.output_handler.STRIKE_SING
        spare_sign = self.output_handler.SPARE_SIGN
        score_line = f"|2, 3|2, 3|2, 3|{strike_sign}   |2, 3|2, 3|2, 3|2, 3|9, {spare_sign}|9, {spare_sign}, 3|"
        expected_output = os.linesep.join([self.header_expected_line, score_line, self.total_expected_line])
        expected_output += os.linesep  # autoadded by print
        with unittest.mock.patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
            self.output_handler.handle((turns, self.basic_total))
            self.assertEqual(mock_stdout.getvalue(), expected_output)

        turn = LastTurn()
        turn.first_roll = LastTurn.ROLL_MAX_VALUE
        turn.second_roll = LastTurn.ROLL_MAX_VALUE - 1
        turn.third_roll = LastTurn.ROLL_MAX_VALUE - turn.second_roll
        turns[9] = turn

        score_line = score_line[:-8] + f"{strike_sign}, {turn.second_roll}, {spare_sign}|"
        expected_output = os.linesep.join([self.header_expected_line, score_line, self.total_expected_line])
        expected_output += os.linesep  # autoadded by print
        with unittest.mock.patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
            self.output_handler.handle((turns, self.basic_total))
            self.assertEqual(mock_stdout.getvalue(), expected_output)

    def test_handle_gutters(self):
        turns = self.turns[:]
        turn = BasicTurn()
        turn.first_roll = BasicTurn.ROLL_MIN_VALUE
        turn.second_roll = self.basic_second_roll
        turns[8] = turn
        turn = BasicTurn()
        turn.first_roll = self.basic_first_roll
        turn.second_roll = BasicTurn.ROLL_MIN_VALUE
        turns[3] = turn
        turn = LastTurn()
        turn.first_roll = BasicTurn.ROLL_MIN_VALUE
        turn.second_roll = BasicTurn.ROLL_MIN_VALUE
        turns[9] = turn

        gutter_sign = self.output_handler.GUTTER_SIGN
        score_line = f"|2, 3|2, 3|2, 3|2, {gutter_sign}|2, 3|2, 3|2, 3|2, 3|{gutter_sign}, 3|{gutter_sign}, {gutter_sign}   |"
        expected_output = os.linesep.join([self.header_expected_line, score_line, self.total_expected_line])
        expected_output += os.linesep  # autoadded by print
        with unittest.mock.patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
            self.output_handler.handle((turns, self.basic_total))
            self.assertEqual(mock_stdout.getvalue(), expected_output)

        turn = LastTurn()
        turn.first_roll = BasicTurn.ROLL_MIN_VALUE
        turn.second_roll = BasicTurn.ROLL_MAX_VALUE
        turn.third_roll = BasicTurn.ROLL_MIN_VALUE
        turns[9] = turn

        spare_sign = self.output_handler.SPARE_SIGN
        score_line = f"|2, 3|2, 3|2, 3|2, {gutter_sign}|2, 3|2, 3|2, 3|2, 3|{gutter_sign}, 3|{gutter_sign}, {spare_sign}, {gutter_sign}|"
        expected_output = os.linesep.join([self.header_expected_line, score_line, self.total_expected_line])
        expected_output += os.linesep  # autoadded by print
        with unittest.mock.patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
            self.output_handler.handle((turns, self.basic_total))
            self.assertEqual(mock_stdout.getvalue(), expected_output)

        turn = LastTurn()
        turn.first_roll = BasicTurn.ROLL_MAX_VALUE
        turn.second_roll = BasicTurn.ROLL_MIN_VALUE
        turn.third_roll = BasicTurn.ROLL_MAX_VALUE
        turns[9] = turn

        strike_sign = self.output_handler.STRIKE_SING
        score_line = f"|2, 3|2, 3|2, 3|2, {gutter_sign}|2, 3|2, 3|2, 3|2, 3|{gutter_sign}, 3|{strike_sign}, {gutter_sign}, {spare_sign}|"
        expected_output = os.linesep.join([self.header_expected_line, score_line, self.total_expected_line])
        expected_output += os.linesep  # autoadded by print
        with unittest.mock.patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
            self.output_handler.handle((turns, self.basic_total))
            self.assertEqual(mock_stdout.getvalue(), expected_output)
