import unittest

from input_parser import InputParser


class TestInputParser(unittest.TestCase):
    def setUp(self) -> None:
        self.game_input_parser = InputParser()

    def test_handle_basic(self):
        input = "2, 3, 5, 4, 9, 1, 10, 3, 2, 4, 2, 3, 3, 4, 6, 10, 3, 2"
        turns = self.game_input_parser.handle(input)
        self.assertEqual(len(turns), 10)
        self.assertEqual(turns[0].first_roll, 2)
        self.assertEqual(turns[0].second_roll, 3)
        self.assertEqual(turns[0].is_strike, False)
        self.assertEqual(turns[0].is_spare, False)

        self.assertEqual(turns[2].is_spare, True)

        self.assertEqual(turns[3].is_strike, True)

        self.assertEqual(turns[-1].calculate_total_point(), 5)

    def test_handle_last_strike(self):
        input = "2, 3, 5, 4, 9, 1, 10, 3, 2, 4, 2, 3, 3, 4, 6, 10, 10, 10, 10"
        turns = self.game_input_parser.handle(input)
        self.assertEqual(turns[-1].calculate_total_point(), 30)

    def test_handle_misses(self):
        input = "2, 3, 5, 4, 9, 0, 0, 0, 0, 2, 4, 2, 3, 3, 4, 6, 10, 10, 2, 0"
        turns = self.game_input_parser.handle(input)
        self.assertEqual(turns[2].second_roll, 0)
        self.assertEqual(turns[3].first_roll, 0)
        self.assertEqual(turns[3].second_roll, 0)
        self.assertEqual(turns[4].first_roll, 0)

    def test_handle_strikes_and_spares(self):
        input = "7, 3, 10, 9, 1, 10, 10, 4, 6, 7, 3, 3, 6, 10, 10, 10, 10"
        turns = self.game_input_parser.handle(input)
        self.assertEqual(turns[0].is_spare, True)
        self.assertEqual(turns[1].is_strike, True)
        self.assertEqual(turns[-2].is_strike, True)
        self.assertEqual(turns[-1].is_strike, True)
