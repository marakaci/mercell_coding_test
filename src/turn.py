import abc
from typing import Optional


class AbstractTurn(abc.ABC):
    """
    Represents a bowling turn that consists of rolls
    2 rolls are always made
    strike is made when 10 points are got in the first turn
    spare is done when 10 points are got in total
    this reflects the calculation of next turns

    """
    ROLL_MAX_VALUE = 10
    ROLL_MIN_VALUE = 0
    _first_roll: Optional[int] = None
    _second_roll: Optional[int] = None

    def __str__(self):
        return f"first_roll: {self.first_roll}, second_roll: {self.second_roll}, " \
               f"is_strike: {self.is_strike}, is_spare: {self.is_spare}" # pragma: no cover

    @property
    def first_roll(self):
        return self._first_roll

    @first_roll.setter
    def first_roll(self, value: int):
        if self.ROLL_MIN_VALUE <= value <= self.ROLL_MAX_VALUE:
            self._first_roll = value
        else:
            raise ValueError(f"first_roll must be from {self.ROLL_MIN_VALUE} to {self.ROLL_MAX_VALUE}")

    @property
    def second_roll(self):
        return self._second_roll

    @second_roll.setter
    def second_roll(self, value: int):
        if self.ROLL_MIN_VALUE <= value <= self.ROLL_MAX_VALUE:
            self._second_roll = value
        else:
            raise ValueError(f"second_roll must be from {self.ROLL_MIN_VALUE} to {self.ROLL_MAX_VALUE}")

    @property
    def is_strike(self):
        return self.first_roll == self.ROLL_MAX_VALUE

    @property
    def is_spare(self):

        return \
            not self.is_strike and \
            self.first_roll is not None and \
            self.second_roll is not None and \
            (self.first_roll + self.second_roll) == self.ROLL_MAX_VALUE

    @property
    def is_first_gutter(self):
        return self.first_roll == self.ROLL_MIN_VALUE

    @property
    def is_second_gutter(self):
        return self.second_roll == self.ROLL_MIN_VALUE

    def calculate_total_point(self) -> int:
        raise NotImplemented # pragma: no cover


class BasicTurn(AbstractTurn):
    """
    Represents a standard turn in the game
    """

    @AbstractTurn.first_roll.setter
    def first_roll(self, value: int):
        if not self.second_roll or (self.second_roll + value <= self.ROLL_MAX_VALUE):
            super(BasicTurn, self.__class__).first_roll.fset(self, value)
        else:
            raise ValueError(f"first_roll + second_roll must be <= than {self.ROLL_MAX_VALUE}")

    @AbstractTurn.second_roll.setter
    def second_roll(self, value: int):
        if self.first_roll + value <= self.ROLL_MAX_VALUE:
            super(BasicTurn, self.__class__).second_roll.fset(self, value)
        else:
            raise ValueError(f"first_roll + second_roll must be <= than {self.ROLL_MAX_VALUE}")

    def calculate_total_point(self) -> int:
        return (self.first_roll or 0) + (self.second_roll or 0)


class LastTurn(AbstractTurn):
    """
    Represents the last turn in the game that has special treatment
    if strike is done 2 more rolls can be made
    if spare is done 1 more roll can be made
    """
    _third_roll: int = None

    def __init__(self, *args, **kwargs):
        super(LastTurn, self).__init__(*args, **kwargs)
        self._third_roll: Optional[int] = None

    def __str__(self):
        base_str = super(LastTurn, self).__str__() # pragma: no cover
        return base_str + f" third_roll: {self.third_roll}" # pragma: no cover

    @AbstractTurn.second_roll.setter
    def second_roll(self, value: int):
        if (self.first_roll + value <= self.ROLL_MAX_VALUE) \
                or self.is_strike:
            # it's possible to make a roll after a strike in the last turn
            super(LastTurn, self.__class__).second_roll.fset(self, value)
        else:
            raise ValueError(f"first_roll + second_roll must be <= than {self.ROLL_MAX_VALUE}")

    @property
    def third_roll(self):
        return self._third_roll

    @third_roll.setter
    def third_roll(self, value: int):
        if self.is_strike or self.is_spare:
            if self.ROLL_MIN_VALUE <= value <= self.ROLL_MAX_VALUE:
                self._third_roll = value
            else:
                raise ValueError(f"third_roll must be from {self.ROLL_MIN_VALUE} to {self.ROLL_MAX_VALUE}")
        else:
            raise ValueError(f"it is only possible to set third roll after strike or spare")

    @property
    def is_second_strike(self):
        "two strikes in a row"
        return self.is_strike and self.second_roll == self.ROLL_MAX_VALUE

    @property
    def is_third_strike(self):
        "three strikes in a row"
        return self.is_second_strike and self.third_roll == self.ROLL_MAX_VALUE

    @property
    def is_second_spare(self):
        return \
            not self.is_second_strike and \
            not self.is_spare and \
            self.second_roll is not None and \
            self.third_roll is not None and \
            (self.second_roll + self.third_roll) == self.ROLL_MAX_VALUE

    @property
    def is_third_gutter(self):
        return self.third_roll == self.ROLL_MIN_VALUE

    def calculate_total_point(self):
        basic_total = (self.first_roll or 0) + (self.second_roll or 0) + (self.third_roll or 0)
        return basic_total
