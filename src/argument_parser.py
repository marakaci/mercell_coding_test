import argparse
from typing import List

from pipeline import Step


class ArgumentParser(Step):
    """Accepts list of args as input as returns file_path out of them"""

    def handle(self, args: List[str]):
        output = self.parse_args(args)
        return output

    def parse_args(self, args: List[str]):
        parser = argparse.ArgumentParser(description='Process bowling input file path')
        parser.add_argument('file_path', type=str, help='path to a file to read bowling input from')
        args = parser.parse_args(args)
        return args.file_path
